/**
 * 
 */

var yadroFunctions = {};

/**
 * @return IntrovertConfig
 */
yadroFunctions.getConfig = function () {
    if (!yadroWidget.config) {
        yadroWidget.config = new IntrovertConfig();
    }

    return yadroWidget.config;
};

yadroFunctions.getBaseUrl = function () {
    return yadroFunctions.getConfig().baseUrl;
};

/**
 * добовляем вкладку прекрепленных элементов в карточку
 *
 * @param {object} type  {
 *      id:"intr_doubles_4455", // должен быть уникальным, используется в качестве имени виджета амо
 *      name:"doubles", // имя вкладки
 *      linked:true, // отображать или нет (необязательно)
 *      sort:1, // порядок (необязательно)
 *      type:"widget" (необязательно)
 *  }
 * @param widget // объект виджета, в котором определены функции для работы с апи карточки
 */
yadroFunctions.addLinkedType = function (type, widget) {
    var func = new function () {
        this.loadPreloadedData = function () {
            return new Promise(function (resolve, reject) {
                resolve({});
            });
        };

        this.loadElements = function () {
            return new Promise(function (resolve, reject) {
                resolve({});
            });
        };

        this.linkCard = function () {
            return new Promise(function (resolve, reject) {
                resolve({});
            });
        };

        this.searchDataInCard = function () {
            return new Promise(function (resolve, reject) {
                resolve({});
            });
        };
    };
    
    widget = widget || func;
    
    try {
        if (
            type.id
            && type.name
            && widget.loadPreloadedData
            && widget.loadElements
            && widget.linkCard
            && widget.searchDataInCard
        ) {
            window.yadroWidget.linkedTypes[type.id] = {type: type, widget: widget};
        }
    } catch (e) {}
};

yadroFunctions.pushLinkedTypes = function () {
    var order = localStorage.getItem('intr_linked_types_order') || '{}';
    order = JSON.parse(order);

    $.each(window.yadroWidget.linkedTypes, function(i, v) {
        var linkedType = {
            sort: 2,
            linked: true,
            type: 'widget'
        };

        $.extend(true, linkedType, v.type);

        var checked = true;
        $.each(AMOCRM.data.current_card._linked_types._linked_types, function(k, type) {
            if (type.id == linkedType.id) {
                checked = false;
            }
        });
        if (checked) {
            if (order[linkedType.id]) {
                linkedType.sort = order[linkedType.id].sort - 1;
            }
            AMOCRM.data.current_card._linked_types._linked_types.push(linkedType);
            var widget = function () {
                this.callbacks = {
                    loadPreloadedData: v.widget.loadPreloadedData,
                    loadElements: v.widget.loadElements,
                    linkCard: v.widget.linkCard,
                    searchDataInCard: v.widget.searchDataInCard,
                    render: function(){return true},
                    init: function(){return true},
                    bind_actions: function(){return true}
                }
            };

            //Widget = require('lib/Widget');
            //new Widget.extend(widget),
            AMOCRM.widgets.list[linkedType.id] = new widget;
            $.extend(AMOCRM.widgets.list[linkedType.id], {
                ns: ".widget:" + linkedType.id,
                init_once: true
            });
        }
    });
};

yadroFunctions.renderLinkedTypes = function () {
    try {
        if (
            !$.isEmptyObject(window.yadroWidget.linkedTypes)
            && AMOCRM.data.is_card
            && AMOCRM.data.current_card
            && AMOCRM.data.current_card._linked_types._linked_types.length > 0
        ) {
            yadroFunctions.pushLinkedTypes();
            //$('.card-fields__linked-toggler-item').remove();
            var _ = require('underscore');
            var linkedTypesView = AMOCRM.data.current_card._linked_types;

            linkedTypesView.render();
            var linkedTypes = _.chain(linkedTypesView._linked_types).sortBy(function (e) {
                return e.sort
            }).filter(function (e) {
                return e.linked
            }).value();

            linkedTypes = linkedTypes.slice(0, 3);
            linkedTypes.reverse();




            if (linkedTypesView.clearToggler) {
                linkedTypesView.clearToggler();
                _.each(linkedTypes, function (e) {
                    linkedTypesView.showTypeAfterClear(e.id)
                }, linkedTypesView)
            } else {
                //новый интерфейс
                linkedTypesView._onRender(linkedTypesView.getLinkedTypesTabs());
            }

        } else if (window.yadroWidget.linkedTypesRenderCount < 5) {
            window.yadroWidget.linkedTypesRenderCount++;
            setTimeout(yadroFunctions.renderLinkedTypes, 500);
        }
    } catch (e) {
        console.log(e);
    }
};

yadroFunctions.getYadroWidget = function() {
    return window.yadroWidget.instance;
};

yadroFunctions.getSocket = function() {
    return window.yadroWidget.widgets.socketWidget;
};

yadroFunctions.parseTime = function (sec) {
    var sec_num = parseInt(sec, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = '0'+ hours;}
    if (minutes < 10) {minutes = '0'+ minutes;}
    if (seconds < 10) {seconds = '0'+ seconds;}

    return (hours != '00' ? hours +':' : '') + minutes +':'+ seconds;
};

/**
 * Возвращает параметры виджета
 * @param {string} code
 * @param {function} [callback] Если указан, то будут запрошены и переданы в callback параметры с сервера,
 * а не локальная копия.
 * @param {function} [onerror] Вызывается в случае ошибки
 * @returns {object} параметры виджета
 */
yadroFunctions.getSettings = function(code, callback, onerror) {
    if (typeof callback == 'function') {
        $.ajax(
            yadroFunctions.getBaseUrl() +'/yadrotrue/_widget/settings.php?key=<?=$_GET["key"];?>&code=' + code,
            {
                //dataType: 'json'
            }
        ).done(function(data) {
            data = data || null;

            if (data) {
                try {
                    data = JSON.parse(data);
                    data = data || {};
                    window.yadroWidget.settings[code] = data;
                } catch (e) {
                    console.error(e);
                }
            } else {
                data = {};
                window.yadroWidget.settings[code] = data;
            }

            callback(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (typeof onerror == 'function') {
                onerror(errorThrown);
            } else {
                console.error(errorThrown);
            }
        });
    } else {
        return window.yadroWidget.settings[code];
    }
};

/**
 * Устанавливает настройки виджета
 * @param {string} code код виджета
 * @param {object} settings объект параметров
 * @param {object} callback
 * @returns {object} параметры виджета
 */
yadroFunctions.setSettings = function(code, settings, callback) {
    window.yadroWidget.settings[code] = window.yadroWidget.settings[code] || {};

    $.each(settings, function (k, v) {
        window.yadroWidget.settings[code][k] = v;
    });

    $.ajax(
        yadroFunctions.getBaseUrl() +'/yadrotrue/_widget/settings.php?key=<?=$_GET["key"];?>&code=' + code,
        {
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify(window.yadroWidget.settings[code])
        }
    ).done(function () {
        if (typeof callback == 'function') {
            callback();
        }
    });

    return window.yadroWidget.settings[code];
};

// Плохо создает модальное окно (без готового HTML)
yadroFunctions.openModal = function(contentClass, callback){
    return new Modal({
        class_name: 'modal-window',
        init: function ($modal_body) {
            $modal_body
                .trigger('modal:loaded')
                .html('<div class="'+contentClass+'"><div style="text-align: center;">Загрузка...</div></div>')
                .trigger('modal:centrify')
                .append('<span class="modal-body__close"><span class="icon icon-modal-close"></span></span>');
            callback($('.'.contentClass));
            yadroFunctions.centrifyModal = function(){
                $modal_body.trigger('modal:centrify')
            }
        },
        destroy: function () {}
    });
};

// Создает модальное окно с готовым HTML
yadroFunctions.renderModal = function(contentClass, html, callback, destroy){
    var Modal = require('lib/components/base/modal');
    return new Modal({
        class_name: 'modal-window',
        init: function ($modal_body) {
            $modal_body
                .trigger('modal:loaded')
                .html('<div class="'+contentClass+'">'+html+'</div>')
                .trigger('modal:centrify')
                .append('<span class="modal-body__close"><span class="icon icon-modal-close"></span></span>');

            yadroFunctions.centrifyModal = function() {
                $modal_body.trigger('modal:centrify');
            };

            if (typeof callback == 'function') {
                callback($('.' + contentClass), $modal_body);
            }
        },
        destroy: function () {
            if (typeof destroy == 'function') {
                destroy();
            }
        }
    });
};

yadroFunctions.getFieldValue = function(id){
    var val = $('.tr_wrapper_'+id).find(':input').val();
    return val || $(':input[name="CFV['+ id +']"]').val();
};

/**
 * @param {object|null} self объект виджета ядра
 * @param {string} widgetClass html class виджета
 * @param {string} html код
 * @param {string} style стили
 * @param {string} imgsrc адрес иконки виджета
 */
yadroFunctions.renderRightWidget = function(self, widgetClass, html, style, imgsrc){
    if (self === null) {
        self = yadroFunctions.getYadroWidget();
    } else if (arguments.length < 5 && typeof self == 'string') {
        var args = [].slice.call(arguments);
        widgetClass = args[0] || '';
        html = args[1] || '';
        style = args[2] || '';
        imgsrc = args[3] || '';
        self = yadroFunctions.getYadroWidget();
    }

    self.render_template({
        caption: { class_name: widgetClass },
        body: '',
        render: '<div class="ac-form">' +
        '	<div class="widget_container">' +
        html +
        '	</div>' +
        '	<style>' +
        style +
        '	</style>' +
        '</div>'
    }, {
        widget_class : widgetClass,
    });
    $('.'+widgetClass).append('<img class="card-widgets__widget__caption__logo" src="' + imgsrc + '" onerror="this.parentNode.removeChild(this)" alt="" style="display: inline;">')
};

/**
 * перенаправляет в карточку элемента
 * @param id элемента
 * @param type тип элемента - имя или номер, можно посмотреть в AMOCRM.element_types
 */
yadroFunctions.goToCard = function(id, type) {
    if ($.isNumeric(type)) {
        $.each(AMOCRM.element_types, function (typeName, typeNumber) {
            if (type == typeNumber) {
                type = typeName;
                return false;
            }
        })
    }

    AMOCRM.router.navigate('/'+ type +'/detail/' + id, {trigger: true})
};

yadroFunctions.reloadCard = function () {
    AMOCRM.data.current_card.reloadCard();
};

yadroFunctions.reloadPage = function () {
    $(document).trigger('page:reload');
};

yadroFunctions.getCurrentUser = function () {
    return AMOCRM.constant('user');
};

yadroFunctions.getCookie = function (name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

yadroFunctions.deleteCookie = function(name, path) {
    yadroFunctions.setCookie(name, '', 1990, 1, 1, path);
    //document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

yadroFunctions.setCookie = function(name, value, exp_y, exp_m, exp_d, path, domain, secure) {
    var cookie_string = name + "=" + escape ( value );

    if (exp_y)
    {
        var expires = new Date ( exp_y, exp_m, exp_d );
        cookie_string += "; expires=" + expires.toGMTString();
    }

    if (path)
        cookie_string += "; path=" + escape(path);

    if (domain)
        cookie_string += "; domain=" + escape(domain);

    if (secure)
        cookie_string += "; secure";

    document.cookie = cookie_string;
};

yadroFunctions.replaceAll = function(search, replacement) {
    var r = search.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    return search.replace(new RegExp(r, 'g'), replacement);
};

//общая функция для добавления раздела в пункте еще в задачах (callback вызывается при клике на раздел)
yadroFunctions.taskMoreBtnAddSection = function(selector, icon, text, callback) {
	selector += '-intr';
	$(document).on('click', '.button-input-wrapper.button-input-more.content__top__action__btn-more', function() {
		if (AMOCRM.data.current_entity == 'todo-line' || AMOCRM.data.current_entity == 'todo-calendar' || AMOCRM.data.current_entity == 'todo') {
			if ($(this).find('.' + selector).length == 0) {
				$(this).find('.button-input__context-menu').append('<li id="" class="button-input__context-menu__item intr_more_btn_section' + selector + '">' +
					'<div class="button-input__context-menu__item__inner">' +
					'<span class="button-input__context-menu__item__icon icon icon-inline ' + icon + '"></span>' +
					'<span class="button-input__context-menu__item__text">' + text + '</span>' +
					'</div>' +
					'</li>'
				);
			}
		}
	});
	$(document).on('click', 'intr_more_btn_section.' + selector, function() {
		callback();
	});
};

//тип 1 в случае успеха, тип 2 в случае ошибки
yadroFunctions.alert = function(text, type) {
    var twig = require('twigjs'),
        modal_html;
    if (!type) type = 1;
    if (type == 1) {
        modal_html = twig({
            ref: '/tmpl/common/modal/success.twig'
        }).render({
            msg: text
        });
    } else if (type == 2) {
        modal_html = twig({
            ref: '/tmpl/common/modal/error.twig'
        }).render({
            text: text,
            no_retry: '1'
        });
    } else {
        console.log('Задан неверный тип');
        return false;
    }
    return new Modal({
        class_name: 'modal-window',
        init: function($modal_body) {
            $modal_body.trigger('modal:loaded').html(modal_html).trigger('modal:centrify')
        },
        destroy: function() {}
    });
};

yadroFunctions.log = function(text, type) {
    var log = yadroFunctions.getCookie('yadro_log');
    if (log != null && (log == 'all' || log == type)) {
        console.log(text);
    }
};

yadroFunctions.logOn = function(active, type) {
    if (!active) {
        yadroFunctions.deleteCookie('yadro_log', '/');
        return;
    }
    if (!type) type = 'all';
    yadroFunctions.setCookie('yadro_log', type, 2020, 1, 1, '/');
};

yadroFunctions.clearCache = function(flag) {
    flag = flag ? 'all' : 'Y';

    return $.ajax(
        yadroFunctions.getBaseUrl() +'/yadrotrue/_widget/loader.php?SUBDOMAIN='+
        AMOCRM.widgets.system.subdomain + '&DOMAIN=' + AMOCRM.widgets.system.domain +
        '&USER_LOGIN=' + AMOCRM.widgets.system.amouser + '&USER_HASH=' + AMOCRM.widgets.system.amohash +
        '&v=' + (new Date()).getDay() +'&clear_cache='+ flag
    );
};

//генерирует уникальный ид
yadroFunctions.uniqid = function(prefix, en) {
    var prefix = prefix || '',
        en = en || false,
        result, us,
        seed = function(s, w) {
            s = parseInt(s, 10).toString(16);
            return w < s.length ? s.slice(s.length - w) :
                (w > s.length) ? new Array(1 + (w - s.length)).join('0') + s : s;
        };
    result = prefix + seed(parseInt(new Date().getTime() / 1000, 10), 8) +
        seed(Math.floor(Math.random() * 0x75bcd15) + 1, 5);
    if (en) result += (Math.random() * 10).toFixed(8).toString();
    return result;
};

/**
 * добавляем вкладку типа группы полей в карточку
 * @param {string} name  	// имя вкладки
 * @param {int} sort  		// какой по счету должна быть вкладка
 * @param {string} html  	// html код
 */
yadroFunctions.addTab = function(name, sort, html, id) {
    if (!id) {
        id = yadroFunctions.uniqid();
    }
    var newtab = {
        '_tab_type': 'groups',
        'id': id,
        'name': name,
        'sort': sort
    };
    AMOCRM.data.current_card.tabs._tabs.splice(sort, 0, newtab);
    $('.card-entity-form__fields').append('<div class="linked-forms__group-wrapper linked-forms__group-wrapper_main js-cf-group-wrapper" data-id="' + id + '" style="display: none">' + html + '</div>');
    AMOCRM.data.current_card.tabs.render();
};

yadroFunctions.getQueryParameters = function(str) {
    return (str || document.location.search).replace(/(^\?)/, '').split('&').map(function(n) {
        return n = n.split('='), this[n[0]] = n[1], this
    }.bind({}))[0];
};

yadroFunctions.renderPipelineSelect = function(selected_pipeline, selected_status, name, id, multiple, pipe_name) {
    if (!name) {
        name = 'lead[STATUS]';
    }
	if (!pipe_name) {
		pipe_name = 'lead[PIPELINE]';
	}
    if (!id) {
        id = 'lead_status_input';
    }
    if (!multiple) {
        multiple = false;
    }
    var twig = require('twig');
    if (!selected_pipeline || !selected_status) {
        for (var a in yadroWidget.pipeline_select_items) {
            selected_pipeline = a;
            break;
        }
        for (var a in yadroWidget.pipeline_select_items[selected_pipeline]['statuses']) {
            selected_status = a;
            break;
        }
        selected_status = yadroWidget.pipeline_select_items[selected_pipeline]['statuses'][selected_status]['id'];
        selected_pipeline = yadroWidget.pipeline_select_items[selected_pipeline]['id'];
    }
    
    var params = {
        'has_pipelines': true,
        'name': name,
        'id': id,
        'class_name': 'card-cf-lead-status-select checkbox_template_settings pipeline_template_settings',
        //'selected_pipeline_id': selected_pipeline,
        //'selected': selected_status,
        //'selected_statuses': selected_pipeline || selected_status,
        'title': 'Воронки',
        'title_numeral': 'Воронка, Воронки, Воронки, Воронки',
        'pipelines_numeral': 'Воронка, Воронки, Воронки, Воронки', //"воронки,воронок,воронок,воронок",
        //'statuses_numeral': "этапа,этапов,этапов,этапов",
        'items': yadroWidget.pipeline_select_items,
        'multiple': multiple,
    };

    if (typeof selected_pipeline == 'object' || typeof selected_pipeline == 'object') {
        //params.selected_statuses = selected_pipeline || selected_status;
        params.sel = selected_pipeline || selected_status;
    } else {
        params.selected_pipeline_id = selected_pipeline;
        params.selected = selected_status;
        params.selected_pipe = {
            name: pipe_name,
        };
    }
    
    return twig({
        ref: '/tmpl/controls/pipeline_select/select.twig'
    }).render(params);
};

yadroFunctions.parseQueryParameters = function(str) {

    // get query string from url (optional) or window
    var queryString = str ? str.replace(/(^\?)/, '') : window.location.search.slice(1);
    queryString = decodeURIComponent(queryString);
    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i=0; i<arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // in case params look like: list[]=thing1&list[]=thing2
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function(v) {
                paramNum = v.slice(1,-1);
                return '';
            });

            // set parameter value (use 'true' if empty)
            var paramValue = typeof(a[1])==='undefined' ? '' : a[1];

            // if parameter name already exists
            if (obj[paramName]) {
                // convert value to array (if still string)
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // if no array index number specified...
                if (typeof paramNum === 'undefined' || paramNum == '') {
                    // put the value on the end of the array
                    obj[paramName].push(paramValue);
                }
                // if array index number specified...
                else {
                    // put the value at that index number
                    obj[paramName][paramNum] = paramValue;
                }
            }
            // if param name doesn't exist yet, set it
            else {
                obj[paramName] = paramValue;
            }
        }
    }

    return obj;
};

/**
 * позволяет получить все поля из заданного контейнера
 * на выходе [{id:..., value:..., name:...}]
 * @param block контейнер jquery пример для карточки :$('#edit_card')
 */
yadroFunctions.getFieldInfo = function(block) {
    var fieldInfo = [];
    if (typeof block != 'object') {
        return fieldInfo;
    }
    $.each(block.find('.linked-form__field'), function() {
        var name = $(this).find('.linked-form__field__label span').html() || false,
            input = false,
            value = '';
        if (
            $(this).hasClass('linked-form__field-text') ||
            $(this).hasClass('linked-form__field-numeric') ||
            $(this).hasClass('linked-form__field-date') ||
            $(this).hasClass('linked-form__field-url') ||
            $(this).hasClass('linked-form__field-textarea') ||
            $(this).hasClass('linked-form__field-address')
        ) {
            input = $(this).find('.linked-form__field__value').find('input, textarea') || false;
            value = input.val();
        }
        if (
            $(this).hasClass('linked-form__field-checkbox')
        ) {
            input = $(this).find('.linked-form__field__value input') || false;
            value = input.is(':checked')?AMOCRM.lang.Yes:AMOCRM.lang.No;
        }
        if (
            $(this).hasClass('linked-form__field-select')
        ) {
            input = $(this).find('.linked-form__field__value input') || false;
            value = $(this).find('.control--select--list--item-selected').find('span').html();
        }
        if (
            $(this).hasClass('linked-form__field-multiselect')
        ) {
            input = $(this).find('.linked-form__field__value input') || false;
            $.each(input, function() {
                if ($(this).is(':checked')) {
                    value += $(this).parent().siblings('.checkboxes_dropdown__label_title').attr('title')+', ';
                }
            });
        }
        if (
            $(this).hasClass('linked-form__field-radio')
        ) {
            input = $(this).find('.linked-form__field__value input') || false;
            $.each(input, function() {
                if ($(this).is(':checked')) {
                    value = $(this).parents('.control-radio__label').attr('title');
                }
            });
        }
        if (
            $(this).hasClass('linked-form__field-smart_address')
        ) {
            input = $(this).find('.linked-form__field__value .address_line_1 input') || false;
            value = input.val();
        }
        if (
            $(this).hasClass('linked-form__field-pei')
        ) {
            // для второго email или телефона амо рендерит имя с переносами строк и кучей пробелов, удаляем их
            name = $(this).find('.linked-form__field__label button span').html().trim() || false,
                // для телефона контакта выбираем элемент со значением, элемент с отформатированным значением отбрасываем
                input = $(this).find('.linked-form__field__value input').not('.control-phone__formatted') || false;
            value = input.val();
        }
        if (input && name) {
            var cfid = input.attr('name') || false;
            if (cfid) {
                var match = cfid.match(/\[(\w+)\]/);
                if (match && match[1]) {
                    var id = match[1];
                    fieldInfo.push({
                        id: id,
                        name: name,
                        value: value,
                    });
                }
            }
        }
    });
    return fieldInfo;
};

/**
 * Добавляет элемент действия в плиточный интерфейс цифровой воронки
 * @param statuses Список статусов (можно передать массив, а можно число)
 * @param title Заголовок действия
 * @param customClass Класс для элемента действия (нужно задать цвет фона)
 * @param callback Коллбек функция
 */
yadroFunctions.renderDigitalPipelineAction = function (statuses, title, customClass, callback) {
    let twigjs = require('twigjs');
    let template = 
        `<div class="digital-pipeline__business_processes digital-pipeline__business_processes_not-free" data-status="" data-id="">
            <div class="digital-pipeline__business_processes-inner js-dp-resize digital-pipeline__business_processes-inner_amocrm digital-pipeline__business_processes-inner_short" data-action="{{ action }}">
                <div class="digital-pipeline__business_processes-inner-wrapper digital-pipeline__business_processes-inner-wrapper_error-container {{ class }}">
                    <span class="digital-pipeline__buisness-process-dragbutton digital-pipeline__buisness-process-dragbutton_first ui-resizable-w">|||</span>
                    <div class="digital-pipeline__short-task-fake">
                        <div class="digital-pipeline__short-task-inner conditions-groups"></div>
                        <div class="digital-pipeline__short-task-inner digital-pipeline__short-task-inner_settings">
                            <span class="digital-pipeline__short-task-title digital-pipeline__short-task-title_hook" title="{{ title }}">{{ title }}</span>
                        </div>
                    </div>
                </div>
                <div class="digital-pipeline__edit-process-container"></div>
            </div>
        </div>`;
    
    if ($.isArray(statuses) && statuses.length) {
        $.each(statuses, function(i, status) { render(status); });
    } else if ($.isNumeric(statuses)) {
        render(statuses);
    }
    
    function render(status) {
        if ($(`.digital-pipeline__item[data-stage-id=${status}]`).find(`.${customClass}`).length) {
            return;
        }

        let html = twigjs({
            data: template
        }).render({ title: title, class: customClass });
        
        let $element = $(`.digital-pipeline__item[data-stage-id=${status}]`).find('.digital-pipeline__add-process:only-child').first().parent('.digital-pipeline__item-inner');
        $element.prepend(html);
        
        $element.children('.digital-pipeline__business_processes').bind( "dragcreate", function(event, ui){
            $(event.target).draggable("disable");
        });
        
        if ($.isFunction(callback)) {
            $element.find('.digital-pipeline__short-task-fake').on('click', function() { 
                let pipeline = AMOCRM.data.current_view.pipeline.id;
                let statusName = $(`.digital-pipeline__item[data-stage-id=${status}]`).find('span.pipeline_status__head_title_inner').text();
                callback(pipeline, status, statusName);
            });
        }
    }
};

/**
 * Удаляет элемент действия в плиточном интерфейсе цифровой воронки
 * @param statuses Список статусов (можно передать массив, а можно число)
 * @param customClass Класс для элемента действия (нужно задать цвет фона)
 */
yadroFunctions.removeDigitalPipelineAction = function (statuses, customClass) {
    if ($.isArray(statuses) && statuses.length) {
        $.each(statuses, function(i, status) { remove(status); });
    } else if ($.isNumeric(statuses)) {
        remove(statuses);
    }

    function remove(status) {
        let $element = $(`.digital-pipeline__item[data-stage-id=${status}]`).find(`.${customClass}`);
        $element.children('.digital-pipeline__short-task-fake').off('click');
        $element.parents('.digital-pipeline__business_processes').remove();
    }
};

/**
 * Добавляет кнопку в список обработчиков цифровой воронки
 * можно передать объект со свойствами равными нижеследующим параметрам, пример: {name: "name", code: "", ...}
 * @param name имя виджета
 * @param code код кнопки и триггера
 * @param callback колбек функция
 * @param button_name
 * @param desc описание
 * @param logo ссылка на картинку, иначе будет стандартная
 * @param sort
 * @param containerCallback
 */
yadroFunctions.addDigitalPipelineHandler = function (name, code, callback, button_name, desc, logo, sort, containerCallback) {
    try {
        yadroWidget.digitalPipelineHandlers = yadroWidget.digitalPipelineHandlers || {};

        if (typeof name == 'object') {
            yadroWidget.digitalPipelineHandlers[name.code] = name;
            callback = name.callback;
            code = name.code;
        } else {
            yadroWidget.digitalPipelineHandlers[code] = {
                name: name,
                code: code,
                desc: desc,
                logo: logo,
                sort: sort,
                button_name: button_name,
                containerCallback: containerCallback
            };
        }

        $(document).on('click', '.intr_digital-pipeline__add-process-button[data-action="' + code + '"]', function (e) {
            var trigger = 'click:dp:' + code;

            $(document).trigger(trigger, [e]);

            if (typeof callback == 'function') {
                callback(e);
            }

            try {
                AMOCRM.data.current_view.metro_modal.destroy();
            } catch (e) {
                yadroFunctions.log(e, 'digital_pipeline');
            }
        });

    } catch (e) {
        yadroFunctions.log(e, 'digital_pipeline');
    }
};

yadroFunctions._handleMetroModal = function (e) {
    if (
        AMOCRM.data.current_entity != 'leads-dp'
        || !yadroWidget.digitalPipelineHandlers
        || Object.keys(yadroWidget.digitalPipelineHandlers).length == 0
    ) {
        return;
    }
    
    var html = null;

    var handlers = [];
    $.each(yadroWidget.digitalPipelineHandlers, function (i, handler) {
        handlers.push(handler);
    });

    function sortHandlers(handlerA, handlerB) {
        return (handlerA.sort || 100) - (handlerB.sort || 100);
    }
    
    handlers.sort(sortHandlers);

    $.each(handlers, function (i, handler) {
        handler.logo = handler.logo || '';
        handler.button_name = handler.button_name || handler.name;
        handler.desc = handler.desc || handler.name;
        
//            if (
//                !handler.logo
//                && yadroWidget.settings.widgets_images
//                && yadroWidget.settings.widgets_images[handler.code]
//            ) {
//                handler.logo = yadroWidget.settings.widgets_images[handler.code]
//            }
        
        var container = 
        '<div class="digital-pipeline__handler-container dp-handler dp-handler-widgets dp-handler-active' +
            ' dp-handler-custom-widgets digital-pipeline__handler-container_'+ handler.code +'" '+
            ' data-handler-id="" data-handler-code="'+ handler.code +'" data-handler-type="single">'+
            '<div class="dp-handler__thumbnail">'+
                '<div class="digital-pipeline__logo dp-handler__logo-background digital-pipeline__logo_'+ handler.code +'">'+
                    '<img src="'+ handler.logo +'">' +
                '</div>'+
                '<div class="digital-pipeline__description">'+
                    '<div class="digital-pipeline__description__rating"></div>'+
                    '<p class="digital-pipeline__description__name h-text-overflow">' + handler.name + '</p>'+
                    '<p class="digital-pipeline__description__type h-text-overflow"></p>' +
                '</div>'+
            '</div>'+
            '<div class="dp-handler__authorization">'+
                '<div class="digital-pipeline__connect-description" style="word-wrap: break-word;">'+
                    (handler.desc || '') +
                '</div>'+
                '<div class="digital-pipeline__process-button-wrapper digital-pipeline__process-button-wrapper-is-authorized">'+
                    '<div class="intr_digital-pipeline__add-process-button" data-action="'+ handler.code +'">'+
                        '<div class="digital-pipeline__add-process-button-title" title="'+ handler.button_name +'">'+
                            handler.button_name +
                        '</div>'+
                    '</div>'+
                    '<div class="digital-pipeline__add-process-container"></div>'+
                '</div>'+
            '</div>'+
        '</div>';
        
        $container = $(container);
        
        if (typeof handler.containerCallback == 'function') {
            handler.containerCallback($container);
        }
        
        if (html) {
            html = html.add($container);
        } else {
            html = $container;
        }
    });
    
    // если нет картинки, то ставим дефолтную
    html.find('.digital-pipeline__logo img[src=""]').attr(
        'style',
        'height: 18px !important; width: auto !important'
    ).attr(
        'src',
        'https://introvert.bz/img/logotype_white.png'
    ).parent().css('background', '#3a4561');
    
    setTimeout(function () {
        var modalBody = $(e.target);
        
        $('.digital-pipeline__handler-container_amocrm').after(html);

        modalBody.find('.intr_digital-pipeline__add-process-button').css({
            "width": "100%",
            "box-sizing": "border-box",
            "padding": "0 23px",
            "border": "solid 1px rgba(0,0,0,.1)",
            "background": "#158fd2",
            "border-radius": "3px",
            "cursor": "pointer"
        });
    }, 10);
};

// Новая версия (весна 2018)
yadroFunctions._handleMetroModalNew = function (e) {
    if (
        AMOCRM.data.current_entity != 'leads-dp'
        || !yadroWidget.digitalPipelineHandlers
        || Object.keys(yadroWidget.digitalPipelineHandlers).length == 0
    ) {
        return;
    }
    
    var html = null;

    var handlers = [];
    $.each(yadroWidget.digitalPipelineHandlers, function (i, handler) {
        handlers.push(handler);
    });

    function sortHandlers(handlerA, handlerB) {
        return (handlerA.sort || 100) - (handlerB.sort || 100);
    }
    
    handlers.sort(sortHandlers);

    let widgetsContainer = 
        `<h2 class="modal-dp__caption modal-dp__caption_yadro_widgets">Yadro Platform</h2>
        <div class="modal-dp__items modal-dp__items_instruments modal-dp__items_yadro_widgets"></div>`;
    $('.modal-dp__caption.modal-dp__caption_widgets').before(widgetsContainer);
    
    $.each(handlers, function (i, handler) {
        handler.logo = handler.logo || '';
        handler.button_name = handler.button_name || handler.name;
        handler.desc = handler.desc || handler.name;
        
        var container = 
            `<div class="modal-dp-item modal-dp-item_instrument" data-handler-code="${handler.code}" data-handler-type="single">
                <div class="modal-dp-item__icon-wrapper modal-dp-item_instrument__icon-wrapper">
                    <div class="modal-dp-item__icon modal-dp-item_instrument__icon">
                        <svg class="svg-icon svg-digital_pipeline--trigger--create_task-dims">
                        </svg>
                    </div>
                </div>
                <h4 class="modal-dp-item__caption modal-dp-item_instrument__caption"></h4>
                <div class="modal-dp-item__button-add-wrapper modal-dp-item_instrument__button-add-wrapper">
                    <button class="modal-dp-item__button-add js-button-input modal-dp-item_instrument__button-add js-add-mailbox intr_digital-pipeline__add-process-button" data-action="${handler.code}" data-source-id="">
                        <div class="modal-dp-item__button-add-plus-wrapper modal-dp-item_instrument__button-add-plus-wrapper">
                            <span class="modal-dp-item__button-add-plus modal-dp-item_instrument__button-add-plus"></span>
                            <span class="modal-dp-item__button-add-caption modal-dp-item_instrument__button-add-caption">${handler.button_name}</span>
                        </div>
                    </button>
                </div>
            </div>`;
        
        $container = $(container);
        
        if (typeof handler.containerCallback == 'function') {
            handler.containerCallback($container);
        }
        
        if (html) {
            html = html.add($container);
        } else {
            html = $container;
        }
    });
    
    // html.find('.modal-dp-item.modal-dp-item_instrument').css('background', '#3a4561');
    
    setTimeout(function () {
        let $yadro_widgets_container = $('.modal-dp__items_yadro_widgets');
        $yadro_widgets_container.append(html);
        $yadro_widgets_container.find('.modal-dp-item.modal-dp-item_instrument').css('background-color', '#3a4561');
    }, 10);
};

yadroFunctions.getYadroId = function () {
    return '<?=client_GetId()?>';
};

/**
 * настройки статусов ы ЦВ
 */
yadroFunctions.addDPSettingsIcon = function (icon) {
    yadroWidget.dpSettingsIcons = yadroWidget.dpSettingsIcons || [];
    yadroWidget.dpSettingsIcons.push(icon);
};

$(document).on('click', '.digital-pipeline__header-inner', function(e) {
    var $wrap = $('.digital-pipeline__item_head-edit');
    var $title = $wrap.find('.digital-pipeline_status__head_title');
    var name = $title.attr('title');
    var id = $wrap.attr('data-stage-id');
    
    if (!yadroWidget.dpSettingsIcons || yadroWidget.dpSettingsIcons.length == 0) {
        return;
    }
    
    var iconshtml = $('<div class="tips__inner custom-scroll"></div>').css({
        'width': '150px',
        'background': '#f9f9fa',
        'padding': '8px',
        'border': '1px solid #ccc',
        'border-radius': '0',
        'text-align': 'left'
    });
    
    $.each(yadroWidget.dpSettingsIcons, function (i, icon) {
        var iconWrap = $('<div class="tips-item js-tips-item" data-id="' + id + '" data-name="' + name + '"></div>').css({
            'display': 'inline-block',
            'width': '15%',
            'padding': '3px 0',
            'margin': '0 2px'
        });
        iconWrap.append(icon);
        iconshtml.append(iconWrap);
    });

    var tipHtml = $('<div class="tips js-tip" style="display: none;"  id="intr-status-edit-tip"></div>').append(iconshtml);

    var btnHtml = '\
    <span class="intr-status-edit-btn" id="intr-status-edit-btn">\
        <svg class="svg-icon svg-common--settings-dims">\
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#common--settings"></use>\
        </svg>\
    </span>';

    setTimeout(function() {
        var $el = $wrap.find('.digital-pipeline__header-inner');
        var $title = $el.find('.digital-pipeline_status__head_title');
        var $title_inner = $el.find('.pipeline_status__head_title_inner');

        $btnHtml = $(btnHtml);
        $title_inner.append($btnHtml.css({
            'left': '1px',
            'padding': '0 5px',
            'position': 'absolute',
            'top': '9px',
            'cursor': 'pointer',
            'line-height': '14px',
        }));

        $title.append(tipHtml);

        $btnHtml.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.digital-pipeline__header-inner').find('#intr-status-edit-tip').toggle();
        });
    }, 1);
});

yadroFunctions.getAccountData = function () {
    if (yadroWidget.accountPromise) {
        return yadroWidget.accountPromise;
    } else {
        yadroWidget.accountPromise = new Promise (function(resolve, reject) {
            $.get('/private/api/v2/json/accounts/current').then(function(data) {
                try {
                    yadroWidget.account = data.response.account;
                    resolve(yadroWidget.account)
                } catch (e) {

                }
            })
        });
    }
};

yadroFunctions.getAccountData();

yadroFunctions.getElementParams = function () {
    try {
        var lead, contact, company, params = {};
        var models = AMOCRM.data.current_card.linked_forms.form_models.models || [];
        models.push(AMOCRM.data.current_card.model);
        
        var getPatterns = function (entity, entityName, name) {
            var params = {};
            
            if (entity.attributes[name +'[MAIN_USER]']) {
                if (AMOCRM.constant('user').id != entity.attributes[name +'[MAIN_USER]']) {
                    try {
                        $.each(yadroWidget.account.users, function (i, user) {
                            if (user.id == entity.attributes[name +'[MAIN_USER]']) {
                                params[entityName + '.Отв-ный.Телефон'] = user.phone_number;
                                params[entityName + '.Ответственный.Телефон'] = user.phone_number;
                                return false;
                            }
                        });
                    } catch (e) {
                        
                    }
                } else {
                    params[entityName + '.Отв-ный.Телефон'] = AMOCRM.constant('user').personal_mobile;
                    params[entityName + '.Ответственный.Телефон'] = AMOCRM.constant('user').personal_mobile;
                }
            }
            
            return params;
        };
     
        $.each(models, function (i, model) {
            switch (model.url) {
                case '/ajax/leads/detail/':
                    lead = model;
                    if (lead) {
                        $.extend(params, getPatterns(lead, 'Сделка', 'lead'));
                    }
                    break;
                case '/ajax/contacts/detail/':
                    contact = model;
                    if (contact) {
                        $.extend(params, getPatterns(contact, 'Контакт', 'contact'));
                    }
                    break;
                case '/ajax/companies/detail/':
                    company = model;
                    if (company) {
                        $.extend(params, getPatterns(company, 'Компания', 'company'));
                    }
                    break;
            }
        });
        
        var getAddressPatterns = function(entity, entityName, field) {
            var params = {};
            var mask = entityName +'.'+ field.NAME;
            
            params[mask +'.Адрес'] = entity['CFV['+ field.ID +'][address_line_1][VALUE]'];
            params[mask +'.Адрес - продолжение'] = entity['CFV['+ field.ID +'][address_line_2][VALUE]'];
            params[mask +'.Город'] = entity['CFV['+ field.ID +'][city][VALUE]'];
            params[mask +'.Регион'] = entity['CFV['+ field.ID +'][state][VALUE]'];
            params[mask +'.Индекс'] = entity['CFV['+ field.ID +'][zip][VALUE]'];
            params[mask +'.Страна'] = field.VARIANTS.country[entity['CFV['+ field.ID +'][country][VALUE]']];
            
            return params;
        };

        var getFieldValue = function (entity, entityName, field) {
            if (field.TYPE_ID == 13) {
                params = $.extend(params, getAddressPatterns(entity.attributes, entityName, field));
            } else if (field.TYPE_ID == 4) {
                var value = entity.attributes['CFV[' + field.ID + ']'];

                try {
                    value = AMOCRM.constant('account').cf[field.ID].ENUMS[value].VALUE;
                } catch (e) {

                }

                params[entityName + '.' + field.NAME] = value;
            } else {
                //params[entityName +'.' + field.NAME] = entity.attributes['CFV[' + field.ID + ']'];
                //params[entityName +'.' + field.ID] = entity.attributes['CFV[' + field.ID + ']'];
            }
        };
        
        var fields = AMOCRM.constant('account').cf;
    
        $.each(fields, function (id, field) {
            switch (field.ELEMENT_TYPES[0]) {
                case 2:
                    if (lead) {
                        getFieldValue(lead, 'Сделка', field);
                    }
                    break;
                    
                case 1:
                    if (contact) {
                        getFieldValue(contact, 'Контакт', field);
                    }
                    break;
                    
                case 3:
                    if (company) {
                        getFieldValue(company, 'Компания', field);
                    }
                    break;
            }
        });
        
        var _params = {};
        
        $.each(params, function (k, v) {
            _params['{{'+ k + '}}'] = v;
            _params['%'+ k + '%'] = v;
        });
    } catch(e) {
        return {};
    }
    
    return _params;
};

/**
 * Добавляет(парсит) шаблон twig
 * @param widgetName имя/код виджета
 * @param tpls объект шаблонов 
 * @example
 * {template_name: template_url, ...}
 */
yadroFunctions.addTwig = function (widgetName, tpls) {
    var promise =  new Promise(function (resolve, reject) {
        yadroWidget.templates[widgetName] = yadroWidget.templates[widgetName] || {};
        yadroWidget.templates['promise'] = yadroWidget.templates['promise'] || {};
        var promises = [];
        // загружаем шаблоны

        var twig = require('twigjs');

        $.each(tpls, function (k, url) {
            var id = "/intr/"+ widgetName +"/"+ k +".twig";

            if (!yadroWidget.templates[widgetName][id]) {
                var promise = $.ajax(url, {cache : false}).done(function (data) {
                    yadroWidget.templates[widgetName][id] = twig({
                        id: id, // любой id
                        data: data,
                        allowInlineIncludes: true,
                    });
                });

                promises.push(promise);
            }
        });

        var promiseAll = Promise.all(promises).then(function (rs, rj) {
            resolve(yadroWidget.templates[widgetName]);
        });
    });

    $.each(tpls, function (k, url) {
        var id = "/intr/" + widgetName + "/" + k + ".twig";
        yadroWidget.templates['promise'][id] = promise;
    });
    
    return promise;
};

/**
 * Генерирует html по шаблону twig
 * @param name имя/id шаблона
 * @param params параметры, передаваемы в шаблон
 * @param func колбек, в который возвращается html
 * 
 * @example
 * yadroFunctions.render('name', {}, function(html) {
 *   // do somethimg with html
 * });
 * 
 */
yadroFunctions.render = function (name, params, func) {
    if (yadroWidget.templates['promise'][name]) {
        yadroWidget.templates['promise'][name].then(function (templates) {
            var html = templates[name].render(params);

            func(html);
            
            delete yadroWidget.templates['promise'][name];
        });
    } else {
        var twig = require('twigjs');
        var html = twig({
            ref: name,
        }).render(params);

        func(html);
    }
};

