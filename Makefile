install:
	npm install

lint:
	npm run eslint ./src/

test:
	npm run test

build:
	rm -rf dist/src/
	npm run build

.PHONY: test
