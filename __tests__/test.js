/* global beforeAll require test describe it expect document */

// import Nightmare from 'nightmare';
import fs from 'fs';
import path from 'path';
// import $ from 'jquery';
import twig from 'twig';

describe('simple widget test', () => {
  const widgetCode = 'MyWidget';

  beforeAll(() => {
    /* eslint-disable global-require */
    global.yadroWidget = {
      widgets: {},
    };
    global.yadroWidget.widgets[widgetCode] = null;
    // require('../lib/yadroFunctions.js');

    const parent = '<div class="test_parent"></div>';
    document.documentElement.innerHTML = parent;
    require('../src/simple.js');
    /* eslint-enable global-require */
  });

  test('simple test', () => {
    const pathFile = './templates/simple.twig';
    const fileData = fs.readFileSync(path.resolve(pathFile), 'utf8');
    const simpleTemplate = twig.twig({
      data: fileData,
    });

    const testData = {
      class_name: 'test1',
      text: 'text1',
    };

    const simpleTemplateRender = simpleTemplate.render(testData);
    // const div = $(simpleTemplateRender);

    global.yadroWidget.widgets[widgetCode].init();
    const element = document.querySelector('.test1').outerHTML;
    expect(element).toBe(simpleTemplateRender);
  });

  // const url = 'https://intrdev.amocrm.ru';
  // const login = 'ivan.gagarinov@introvert.bz';
  // const password = '12345678';
  // const page = Nightmare({show: false})
  //   .goto(url)
  //   .wait('#name', '#password')
  //   .insert('#name', login)
  //   .insert('#password', password)
  //   .click('#auth_submit')
  //   .wait('#left_menu');

  // afterAll(page.end);

  // test(
  //   'test dashboard',
  //   async done => {
  //     const bodyClass = await page
  //       .click('[data-entity="dashboard"]')
  //       .wait(3000)
  //       .evaluate(() => document.body.getAttribute('class'));

  //     expect(bodyClass).toBe('dashboard');

  //     await page.end();
  //     done();
  //   },
  //   15000,
  // );
});
