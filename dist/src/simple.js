'use strict';

/* global $ window document yadroWidget yadroFunctions */
/* eslint new-cap: ["error", { "newIsCap": false }] */
/* eslint-disable no-console */

var $ = require('jquery');
var MyWidget = function MyWidget() {
  var div = $('<div class="test">TEST</div>');
  var widget = this;
  this.code = null;

  this.bind_actions = function () {};

  this.render = function () {};

  this.init = function () {
    return '<div class="test">TEST</div>';
  };

  this.renderConfig = function () {};

  this.bootstrap = function (code) {};

  this.log = function () {
    for (var _len = arguments.length, text = Array(_len), _key = 0; _key < _len; _key++) {
      text[_key] = arguments[_key];
    }

    var logger = console.log; //message => yadroFunctions.log(message, widget.code);
    text.forEach(logger);
  };

  this.tick = function (delay, count, func, terminate) {
    //widget.log('tick rest:', count);
    if (count <= 0 || terminate()) {
      func();
    } else {
      setTimeout(function () {
        return widget.tick(delay, count - 1, func, terminate);
      }, delay);
    }
  };
};

yadroWidget.widgets.MyWidget = new MyWidget();