/* global $ window document yadroWidget yadroFunctions */
/* eslint new-cap: ["error", { "newIsCap": false }] */
/* eslint-disable no-console */

import fs from 'fs';
import path from 'path';
import $ from 'jquery';
import twig from 'twig';

const MyWidget = function() {
  const widget = this;
  this.templatesPath = './templates/';
  this.code = null;
  this.templates = {};

  this.bind_actions = () => {};

  this.render = () => {};

  this.init = () => {
    const pathFile = `${widget.templatesPath}simple.twig`;
    widget.addTemplate('simple', pathFile);
    const testData = {
      class_name: 'test1',
      text: 'text1',
    };
    const html = widget.templates.simple.render(testData);
    $('.test_parent').append(html);
    return true;
  };

  this.renderConfig = () => {};

  this.bootstrap = code => {};

  this.log = (...text) => {
    const logger = console.log; // message => yadroFunctions.log(message, widget.code);
    text.forEach(logger);
  };

  this.tick = (delay, count, func, terminate) => {
    if (count <= 0 || terminate()) {
      func();
    } else {
      setTimeout(() => {
        return widget.tick(delay, count - 1, func, terminate);
      }, delay);
    }
  };

  this.addTemplate = (name, pathToFile) => {
    const data = fs.readFileSync(path.resolve(pathToFile), 'utf8');
    const id = `/${widget.code}/${name}`;
    widget.templates[name] = twig.twig({
      id,
      data,
      allowInlineIncludes: true,
    });
  };
};

yadroWidget.widgets.MyWidget = new MyWidget();
